var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Database = require('arangojs').Database;

var encrypt = require('./routes/encrypt');
var decrypt = require('./routes/decrypt');
var db = new Database();
var col;
var pi;
db.createDatabase('rekry2018', function (err) {
  if (!err) {
    console.log('Database created', db.useDatabase('rekry2018'));
    col = db.collection('log').create().then(() => {
      console.log('created collection log');
    }, err => {
      console.log(err);
    });
    db.collection('pi').document('pi').then(doc => {
      pi = doc.pi.toString();
    }, err => {
      db.collection('pi').create().then(res => {
        console.log(res);
        db.collection('pi').save({ _key:'pi', pi: "31415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624406566430860213949463952247371907021798609437027705392171762931767523846748184676694051320005681271452635608277857713427577896091736371787214684409012249534301465495853710507922796892589235420199561121290219608640344181598136297747713099605187072113499999983729780499510597317328160963185950244594553469083026425223082533446850352619311881710100031378387528865875332083814206171776691473035982534904287554687311595628638823537875937519577818577805321712268066130019278766111959092164201989" }).then(meta => {
          console.log(meta);
          pi = db.collection('pi');
        });
      }, err => {
        console.log(err);
      });
    });;
  }
  else {
    console.error('Failed to create database:', err);
    db.useDatabase('rekry2018');
    col = db.collection('log');
    pi = db.collection('pi');
  }
});


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.set('db', db);
app.set('col', col);
app.set('pi', pi);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/decrypt', decrypt);
app.use('/encrypt', encrypt);



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});



// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
