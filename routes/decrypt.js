var express = require('express');
var Database = require('arangojs').Database;
var router = express.Router();
var logItem;
var logI;
var log;

router.post('/', (req, res, next) => {
  var db = new Database();
  db.useDatabase('rekry2018');
  var col = db.collection('log');
  var pi;
  db.collection('pi').document('pi').then(result => {
    pi = result.pi.toString();

    var body = req.body;
    var amount = parseInt(body.decryptAmount);
    var cypher = req.body.decryptString;
    var plainText = "";

    if (cypher && amount.toString() === body.decryptAmount) {
      var i = 2;
      var salt = "";
      for (var c of cypher) {
        if (i % 2 !== 0) {
          plainText = String.fromCharCode(c.charCodeAt(0) - (amount % 26)) + plainText;
        } else {
          salt = c + salt;
        }
        i++;
      }
      if (salt !== pi.substring(0, salt.length)) {
        res.send(400);
      }
      db.query(' for l in log return l ').then(
        cursor => cursor.all()
      ).then(result => {
        logI = result ? result.length + 1 : 0;
        log = result;
        logItem = {
          logId: logI,
          logType: 'decrypt',
          logBefore: cypher,
          logAfter: plainText,
          logSeed: amount
        }
        col.save(logItem).then(meta => {
          db.query(' for l in log sort l.logId desc limit 0,100 return l ').then(
            cursor => cursor.all()
          ).then(result2 => {
            for (var r of result2) {
              delete r['_key'];
              delete r['_id'];
              delete r['_rev'];
            }
            res.send({
              decryptResult: plainText,
              log: result2,
            });
          });
        });
      });
    } else {
      res.send(400);
    }
  });
});

module.exports = router;