var express = require('express');
var Database = require('arangojs').Database;
var router = express.Router();
var logItem;
var logI;

router.post('/', function (req, res, next) {
    var db = new Database();
    db.useDatabase('rekry2018');
    var col = db.collection('log');
    var pi;
    db.collection('pi').document('pi').then( result => {
        console.log(result.pi.toString());
        pi = result.pi.toString();
        var body = req.body;
        var amount = parseInt(body.encryptAmount);
        var eStr = body.encryptString;
        if (eStr && amount.toString() === req.body.encryptAmount) {
            var i = 0;
            var cypher = "";
            for (var s of eStr) {
                cypher = pi.charAt(i) + String.fromCharCode((amount % 26) + s.charCodeAt(0)) + cypher;
                i++; 
            }
            //cypher = resArr.reverse().join("");
            db.query(' for l in log return l ').then(
                cursor => cursor.all()
            ).then(result => {
                logI = result ? result.length + 1 : 0;
                log = result;
                
                logItem = {
                    logId: logI,
                    logType: 'encrypt',
                    logBefore: eStr,
                    logAfter: cypher,
                    logSeed: amount
                }
                col.save(logItem).then(meta => {
                    db.query(' for l in log sort l.logId desc limit 0,100 return l ').then(
                        cursor => cursor.all()
                    ).then(result2 => {
                        for (var r of result2) {
                            delete r['_key'];
                            delete r['_id'];
                            delete r['_rev'];
                        }
                        res.send({
                            encryptResult: cypher,
                            log: result2,
                        });
                    });
                });
            });
        } else {
            res.send(400);
        }
    }) ;
});
module.exports = router;